﻿CREATE TABLE [dbo].[Email]
(	
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[EmailAddress] varchar(1024) NOT NULL,
	CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [Person_Email_FK] FOREIGN KEY([PersonId]) 
	  REFERENCES [dbo].[Person] ([Id])
	  ON UPDATE CASCADE
	  ON DELETE CASCADE
)
