﻿namespace WebSharperProject
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open IntelliFactory.WebSharper.Formlet
open IntelliFactory.WebSharper.JQuery
open Microsoft.FSharp.Linq
open Microsoft.FSharp.Linq.Query
open System.Linq;

module PersonList =
    module Server =
        [<Rpc>]
        let GetListData (str) = 
            let db = new Model.WebSharperDemoDataContext()
            let persons = Queryable.AsQueryable<Model.Person>(db.Persons)
            let q1 = 
                query <@ seq { 
                    for p in persons do 
                        if p.Name.Contains(str) then 
                            yield p.Id, p.Name, p.Age.GetValueOrDefault() } @>
            let a = q1.ToArray()
            a

    module Client =
        module M = Microsoft.FSharp.Collections.Map         
        
        [<JavaScript>]
        let Main () =
            let textBox = Input [Attr.Type "Text"]
            let getText () : string = 
                JQuery.JQuery.Of(textBox.Dom).Text()
            let resDiv = Div []
            let getResults () =
                resDiv.Clear()
                let ul : Element = 
                    textBox.Value
                    |> Server.GetListData
                    |> Array.map (fun (id, name, age) ->
                        let details = Div []
                        let header = Div [name + " (" + age.ToString() + ")" |> Text]
                        PersonListDetails.Client.TogglePanel(id, header)
                    )
                    |> UL
                resDiv.Append(ul)

            textBox |> OnKeyUp (fun e arg -> getResults()) |> ignore
            getResults () 
            Div [
                Span [Text "Seach"]
                textBox
                resDiv
            ]