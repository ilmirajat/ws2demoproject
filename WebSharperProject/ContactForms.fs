﻿namespace WebSharperProject

module ContactForms =
    open IntelliFactory.WebSharper
    open IntelliFactory.WebSharper.Html
    open IntelliFactory.WebSharper.Formlet
    open IntelliFactory.WebSharper.JQuery
    open Model

    type BasicInfo = 
        {
            Name : string
            Age : int 
        }

    type Address = 
        { 
            Street : string
            City : string
            Country : string 
        }

    type Contact =
        | PhoneContact of int
        | EmailContact of string
        | AddressContact of Address
    
    [<Rpc>]
    let GetDetData (info : BasicInfo, con : Contact) = 
        use db = new Model.WebSharperDemoDataContext()
        let insertData = 
            let person = Model.Person() 
            let ageInt = System.Nullable<int>(info.Age)
            person.Name <- info.Name
            person.Age <- ageInt
            db.Persons.InsertOnSubmit(person)
            match con with
            | AddressContact add -> 
                let address = new Model.Address()
                address.Street <- add.Street
                address.City <- add.City 
                address.Country <- add.Country
                person.Addresses.Add(address)
                db.Addresses.InsertOnSubmit(address)
            | PhoneContact phon -> 
                let phone = new Model.Phone()
                phone.PhoneNumber <- phon
                person.Phones.Add(phone)
                db.Phones.InsertOnSubmit(phone)
            | EmailContact e -> 
                let email = new Model.Email()
                email.EmailAddress <- e
                person.Emails.Add(email)
                db.Emails.InsertOnSubmit(email)
        insertData
        db.SubmitChanges()
        "Thank you! You information is is retrieved"

    [<JavaScript>]
    let input (label: string) (err: string) = 
        Controls.Input ""
        |> Validator.IsNotEmpty err
        |> Enhance.WithValidationIcon
        |> Enhance.WithTextLabel label

    [<JavaScript>]
    let inputInt (label: string) (err: string) = 
        Controls.Input ""
        |> Validator.IsGreaterThan "0" err
        |> Validator.IsInt err
        |> Enhance.WithValidationIcon
        |> Enhance.WithTextLabel label
        |> Formlet.Map int

    [<JavaScript>]
    let inputEmail (label: string) (err: string) = 
        Controls.Input ""
        |> Validator.IsEmail err
        |> Enhance.WithValidationIcon
        |> Enhance.WithLabelAndInfo label "Input here your email address"

    [<JavaScript>]
    let BasicInfoForm : Formlet<BasicInfo> =
        Formlet.Yield (fun name age -> { Name = name; Age = age })
        <*> input "Name" "Please enter your name"
        <*> inputInt "Age" "Please enter a valid age"

    [<JavaScript>]
    let ContactInfoForm =
        let phone = 
            inputInt "Phone" "Empty phone number not allowed"
            |> Formlet.Map PhoneContact
        let email = 
            inputEmail "Email" "Email is incorrect format."
            |> Formlet.Map EmailContact
        let address =
            Formlet.Yield (fun str cty ctry ->
                AddressContact {
                    Street = str
                    City = cty
                    Country = ctry 
                })
            <*> input "Street" "Empty street not allowed"
            <*> input "City" "Empty city not allowed"
            <*> input "Country" "Empty country not allowed"
        Formlet.Do {
            let options = 
                [
                    "Phone", phone
                    "Address", address
                    "Email", email
                ]
            let! via = Controls.RadioButtonGroup None options
            return! via
        }



    [<JavaScript>]
    let SignupSequence =
        let infoForm =
            BasicInfoForm
            |> Enhance.WithSubmitAndResetButtons
            |> Enhance.WithErrorSummary "Errors"
            |> Enhance.WithCustomFormContainer {
                 Enhance.FormContainerConfiguration.Default with
                    Header = 
                        "Step 1 - Your name and age" 
                        |> Enhance.FormPart.Text 
                        |> Some
                    Description = 
                        "Please enter your name and age below." 
                        |> Enhance.FormPart.Text 
                        |> Some
               }
        let contactForm =
            ContactInfoForm
            |> Enhance.WithSubmitAndResetButtons
            |> Enhance.WithCustomFormContainer {
                 Enhance.FormContainerConfiguration.Default with
                    Header = 
                        "Step 2 - Your preferred contact information" 
                        |> Enhance.FormPart.Text 
                        |> Some
                    Description = 
                        "Please enter your phone number or your address below." 
                        |> Enhance.FormPart.Text 
                        |> Some 
               }
        let proc (info:BasicInfo) (contact:Contact) () = 
            let content = Div []
            let result =
                match contact with
                | AddressContact address ->
                    "the address: " + address.Street + ", " +
                    address.City + ", " + address.Country
                | PhoneContact phone ->
                    "the phone number: " + phone.ToString()
                | EmailContact email->
                    "the email: " + email

            FieldSet [
                Legend [Text "Sign-up summary"]
                P ["Hi " + info.Name + "!" |> Text]
                P ["You are " + string info.Age + " years old" |> Text]
                P ["Your preferred contact method is via " + result |> Text]
                Input [Attr.Id "Test"; Attr.Type "button"; Attr.Value "Send to Server"]  
                |>! OnClick (fun e args -> 
                    JQuery.Of(e.Dom).FadeOut(5.0) |> ignore
                )
                |>! OnClick (fun e args -> 
                    let res = GetDetData(info, contact)
                    let mutable url = "default.aspx?" + System.DateTime.Now.Ticks.ToString()
                    let elem = 
                        Div [
                            H3 [res |> Text]
                            Hr []
                            A [Attr.HRef url; Text "Create New"]
                        ]
                    JQuery.Of(content.Dom).Append(elem.Dom) |> ignore
                )
                content]
            
        let flow =
            Formlet.Do {
                let! i = infoForm
                let! c = contactForm
                let! confirmatinFrom = Formlet.OfElement (proc i c)
                return! Formlet.OfElement (proc i c)
            }
            |> Formlet.Flowlet
        Div [H1 [Text "Sign up today!"]] -< [flow]